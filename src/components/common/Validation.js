const isFormEmpty = (...filds) => {
  return filds.some(i => {
    return i.length === 0;
  });
};

const isPasswordValid = (...filds) => {
  if (filds[0].length < 6 || filds[1].length < 6) {
    return false;
  } else if (filds[0] !== filds[1]) {
    return false;
  } else {
    return true;
  }
};

export default { isFormEmpty, isPasswordValid };
