import { combineReducers } from "redux";
import User_reducer from "./User_reducer";
import Channel_reducer from "./Channel_reducer";

const rootReducer = combineReducers({
  user: User_reducer,
  channel: Channel_reducer
});

export default rootReducer;
